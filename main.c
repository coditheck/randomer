# include <stdlib.h>
# include <stdio.h>
# include <time.h>

# define N		8
# define DELAI 	1   // en seconde

typedef unsigned char uchar;

/**
 * configuration :
 */

typedef struct {
	int cpu_delai;
 	int seq_count;

} Setting;

/**
 * Fonction
 */
int cpu_inst_count(const int timeout);
char* gennumb(const int seed, const int count);

/**
 * Fonction principale :
 */
int main(int argc, char** argv) {
	Setting settup;

	// parametrage :
	settup.seq_count = (argc > 1)? atoi(argv[1]) : N;
	settup.cpu_delai = (argc > 2)? atoi(argv[2]) : DELAI;

	// traitement :
	int crytoseed = cpu_inst_count(settup.cpu_delai);
	char* data    = gennumb(crytoseed, settup.seq_count);

	printf("%s\n", data);

	// on libere les memoires :
	free(data);

	return EXIT_SUCCESS;
}

/** 
 * Fonction qui permet de compter le nombre d'instructions
 * executees par le processeur durant un laps de temps [timeout]
 * donne.
 */
int cpu_inst_count(const int timeout) {
	int k = 0;
	int begin = time(NULL);
	
	while (time(NULL) - begin < timeout) 
		k = k + 3;

	return k;	

}

/** 
 * Fonction qui permet de generer [count] chiffres entre 0 et 9.
 */
char* gennumb(const int seed, const int count) {
	char* seq = (char*)malloc((count + 1) * sizeof(char));

	if (seq) {
		srand(seed);

		for (int i = 0; i < count; i++)
			seq[i] = (uchar)(48 + (rand() % 10));

		seq[count] = '\0';

		return seq;
	}
 
	return NULL;
}



